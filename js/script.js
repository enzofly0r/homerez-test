$(document).ready(function(){
  /*
  * rotate logo dom load
  */
  $("#logo").hide();
  $("#logo").fadeIn(1000);
  // $("#logo").addClass("scale-animation");

  //// change background
  var images = [];
  images[0] = "webgallery/background.jpg";
  images[1] = "webgallery/background2.jpg";
  images[2] = "webgallery/background3.jpg";
  images[3] = "webgallery/background4.jpg";
  images[4] = "webgallery/background.jpg";
  setInterval(changeBackground, 3000);

  var i = 0;
  function changeBackground(){
    if(i == 5){
      i = 0;
    }
    else{
      $('.layout').css('backgroundImage', 'url('+images[i]+')');
    }
    i++;
  }

  $(".open-form-contact").click(function(){
    $(".open-form-contact").hide();
    $(".form-contact").fadeIn();
  })

  // send message & check if empty is empty
  $("#send-message").click(function(){
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var email = $("#email").val();
    var number = $("#number").val();
    var propertyType = $("#propertyType").val();
    var propertyLocation = $("#propertyLocation").val();
    var propertyMaxOccupancy = $("#propertyMaxOccupancy").val();

    if((fname == "") || (lname == "") || (email == "") || (number == "") || (propertyType == "") || (propertyLocation == "") || (propertyMaxOccupancy == "")){
      swal("Erreur!", "Merci de remplir tous les champs!", "error");
    }
    else{
      swal("Good job!", "Formulaire de contact envoyé.!", "success");

    }
  })

});
